﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Server.Api.Models
{
    public class Account
    {
        [Key] [Required] public int Id { get; set; }

        [Required] public int UserId { get; set; }

        [Required] public string Url { get; set; }

        [Required] public string Username { get; set; }

        [Required] public string Password { get; set; }

        public string Group { get; set; }
    }
}