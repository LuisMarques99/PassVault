import { Injectable } from '@angular/core';
import { HttpClient } from "@angular/common/http";
import { Router } from "@angular/router"
import { Password } from "./models/password";
import { BehaviorSubject, Observable } from 'rxjs';
import { map, tap, catchError } from 'rxjs/operators';
import { HttpErrorResponse, HttpHeaders } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class PasswordService {

  httpOptions = {
    headers: new HttpHeaders({ 'Content-Type': 'application/json' })
  };
  private wtv;
  private passwordSubject: BehaviorSubject<Password>;
  public user: Observable<Password>;

  private baseURL = "https://localhost:5731/api/v1"
  private _passwordUrl = "/accounts"

  //private _groupUrl = "http://localhost:4200/api/groups"

  constructor(
    private router: Router,
    private http: HttpClient
    ) {

    }

  getAllPasswords(){
    return this.http.get<Password[]>(this.baseURL + this._passwordUrl)
      .subscribe(
        data => {
          this.wtv = data;
          console.log(data)
        }
      )
  }

  postPassword(password:Password):Observable<Password>{
    return this.http.post<Password>(this.baseURL+this._passwordUrl, password, this.httpOptions).pipe(
      tap((password: Password)=> console.log("added password: " + password.id)),
      catchError(this.errorHandler))
  }

  errorHandler(error: HttpErrorResponse) {
    return Observable.throw(error.message || "server error.");
  }

  allPasswords(){}
  getPasswordById(id:string){
    return this.http.get<Password>(`/rotaApi/${id}`)
  }

  deletePassword(id:string){
  }
}
