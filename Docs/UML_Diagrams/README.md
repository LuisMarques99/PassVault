# UML Diagrams

## Use Case - Web App

<img src="UML_UseCase_WebApp.png" alt="UseCase_WebApp" width="700"/>

## Web API Architecture

<img src="UML_WebAPIArchitecture.png" alt="WebAPIArchitecture" width="700"/>
