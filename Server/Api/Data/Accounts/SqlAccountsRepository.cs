﻿using System;
using System.Collections.Generic;
using System.Linq;
using Server.Api.Helpers.Exceptions;
using Server.Api.Models;

namespace Server.Api.Data.Accounts
{
    public class SqlAccountsRepository : IAccountsRepository
    {
        private readonly DatabaseContext _context;

        public SqlAccountsRepository(DatabaseContext context)
        {
            _context = context;
        }

        public bool SaveChanges()
        {
            return _context.SaveChanges() > 0;
        }

        public IEnumerable<Account> GetAllAccounts()
        {
            return _context.Accounts.ToList();
        }

        public IEnumerable<Account> GetAllAccountsByUserId(int userId)
        {
            if (!IsUserValid(userId)) throw new InvalidUserException("There is no user with this userId.");

            return _context.Accounts.Where(account => account.UserId.Equals(userId)).ToList();
        }

        public Account GetAccountById(int id)
        {
            return _context.Accounts.SingleOrDefault(account => account.Id.Equals(id));
        }

        public void CreateAccount(Account account)
        {
            if (account == null) throw new ArgumentNullException(nameof(account));

            if (!IsUserValid(account.UserId)) throw new InvalidUserException("There is no user with this userId.");

            if (IsAccountDuplicate(account)) throw new DuplicateAccountException("This password already exists for this userId.");

            _context.Accounts.Add(account);
        }

        public void UpdateAccount(Account account)
        {
            if (!IsUserValid(account.UserId)) throw new InvalidUserException("There is no user with this userId.");

            if (IsAccountDuplicate(account)) throw new DuplicateAccountException("This password already exists for this userId.");
        }

        public void DeleteAccount(Account account)
        {
            if (account == null) throw new ArgumentNullException(nameof(account));

            _context.Accounts.Remove(account);
        }

        private bool IsAccountDuplicate(Account account)
        {
            var duplicateAccount = _context.Accounts.SingleOrDefault(other => other.UserId.Equals(account.UserId) &&
                                                                              other.Url.Equals(account.Url) &&
                                                                              other.Username.Equals(account.Username) &&
                                                                              !other.Id.Equals(account.Id));
            return duplicateAccount != null;
        }

        private bool IsUserValid(int userId)
        {
            var user = _context.Users.SingleOrDefault(other => other.Id.Equals(userId));

            return user != null;
        }
    }
}