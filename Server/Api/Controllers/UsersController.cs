using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;
using System.Text.RegularExpressions;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.JsonPatch;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using Server.Api.Data.Users;
using Server.Api.Dtos.Users;
using Server.Api.Helpers;
using Server.Api.Helpers.Exceptions;
using Server.Api.Models;
using bCrypt = BCrypt.Net.BCrypt;

namespace Server.Api.Controllers
{
    [Authorize]
    [ApiController]
    [Route("api/v1/[controller]")]
    public class UsersController : ControllerBase
    {
        private readonly IUsersRepository _repository;
        private readonly IMapper _mapper;
        private readonly AppSettings _appSettings;

        public UsersController(IUsersRepository repository, IMapper mapper, IOptions<AppSettings> appSettings)
        {
            _repository = repository;
            _mapper = mapper;
            _appSettings = appSettings.Value;
        }
        
        // GET api/v1/users
        [HttpGet]
        public ActionResult<IEnumerable<UserReadDto>> GetAllUsers()
        {
            var users = _repository.GetAllUsers();
            var usersReadDto = _mapper.Map<IEnumerable<UserReadDto>>(users);

            return Ok(usersReadDto);
        }

        // GET api/v1/users/{id}
        [HttpGet("{id}", Name = "GetUserById")]
        public ActionResult<UserReadDto> GetUserById(int id)
        {
            var user = _repository.GetUserById(id);
            var userReadDto = _mapper.Map<UserReadDto>(user);

            if (user != null) return Ok(userReadDto);

            return NotFound();
        }

        // POST api/v1/users
        [AllowAnonymous]
        [HttpPost]
        public ActionResult<UserReadDto> CreateUser(UserCreateDto userCreateDto)
        {
            if (userCreateDto == null) return NoContent();

            userCreateDto.Username = Regex.Replace(userCreateDto.Username, @" ", "");
            userCreateDto.Password = HashPassword(userCreateDto.Password);

            var user = _mapper.Map<User>(userCreateDto);

            try
            {
                _repository.CreateUser(user);
            }
            catch (DuplicateUserException e)
            {
                var message = e.Message;
                return BadRequest(new {message = $"{message} is already in use."});
            }

            _repository.SaveChanges();

            var userReadDto = _mapper.Map<UserReadDto>(user);

            return CreatedAtRoute(nameof(GetUserById), new {userReadDto.Id}, userReadDto);
        }

        // PUT api/v1/users/{id}
        [HttpPut("{id}")]
        public ActionResult UpdateUser(int id, UserUpdateDto userUpdateDto)
        {
            var user = _repository.GetUserById(id);

            if (user == null) return NotFound();

            userUpdateDto.Username = Regex.Replace(userUpdateDto.Username, @" ", "");

            _mapper.Map(userUpdateDto, user);

            if (_repository.GetUserByUsername(user.Username) != null)
                return BadRequest(new {message = "Username is already in use."});
            if (_repository.GetUserByEmail(user.Email) != null)
                return BadRequest(new {message = "Email is already in use."});

            _repository.UpdateUser(user);
            _repository.SaveChanges();

            return NoContent();
        }

        // PATCH api/v1/users/{id}
        [HttpPatch("{id}")]
        public ActionResult PartialUpdateUser(int id, JsonPatchDocument<UserPartialUpdateDto> patchDocument)
        {
            var user = _repository.GetUserById(id);

            if (user == null) return NotFound();

            var userPartialUpdateDto = _mapper.Map<UserPartialUpdateDto>(user);

            patchDocument.ApplyTo(userPartialUpdateDto, ModelState);

            if (!TryValidateModel(userPartialUpdateDto)) return ValidationProblem(ModelState);

            if (userPartialUpdateDto.Username != user.Username)
            {
                userPartialUpdateDto.Username = Regex.Replace(userPartialUpdateDto.Username, @" ", "");
                if (_repository.GetUserByUsername(userPartialUpdateDto.Username) != null)
                    return BadRequest(new {message = "Username is already in use."});
            }

            if (userPartialUpdateDto.Email != user.Email)
            {
                if (_repository.GetUserByEmail(userPartialUpdateDto.Email) != null)
                    return BadRequest(new {message = "Email is already in use."});
            }

            if (userPartialUpdateDto.Password != user.Password)
                userPartialUpdateDto.Password = VerifyPassword(userPartialUpdateDto.Password, user.Password)
                    ? user.Password
                    : HashPassword(userPartialUpdateDto.Password);

            _mapper.Map(userPartialUpdateDto, user);

            _repository.UpdateUser(user);
            _repository.SaveChanges();

            return NoContent();
        }

        // DELETE api/v1/users/{id}
        /*[HttpDelete("{id}")]
        public ActionResult DeleteUser(int id)
        {
            var user = _repository.GetUserById(id);

            if (user == null) return NotFound();

            _repository.DeleteUser(user);
            _repository.SaveChanges();

            return NoContent();
        }*/

        // POST api/v1/users/authenticate
        [AllowAnonymous]
        [HttpPost("authenticate")]
        public ActionResult<UserAuthenticateResponseDto> AuthenticateUser(UserAuthenticateRequestDto request)
        {
            var user = _repository.AuthenticateUser(request.Username, request.Password);

            if (user == null || !request.Username.Equals(user.Username) && !request.Username.Equals(user.Email))
                return BadRequest(new {message = "Username or password is incorrect"});

            var tokenString = JwtToken(user);

            var response = _mapper.Map<UserAuthenticateResponseDto>(user);
            response.Token = tokenString;

            return Ok(response);
        }

        private static string HashPassword(string password)
        {
            return password == null ? "" : bCrypt.HashPassword(password);
        }

        private static bool VerifyPassword(string password, string passwordHash)
        {
            return bCrypt.Verify(password, passwordHash);
        }

        private string JwtToken(User user)
        {
            var tokenHandler = new JwtSecurityTokenHandler();
            var key = Encoding.ASCII.GetBytes(_appSettings.Secret);
            var tokenDescriptor = new SecurityTokenDescriptor
            {
                Subject = new ClaimsIdentity(new[]
                {
                    new Claim(ClaimTypes.Name, user.Id.ToString())
                }),
                Expires = DateTime.UtcNow.AddDays(1),
                SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(key), SecurityAlgorithms.HmacSha256Signature)
            };
            var token = tokenHandler.CreateToken(tokenDescriptor);

            return tokenHandler.WriteToken(token);
        }
    }
}