﻿using System;

namespace Server.Api.Helpers.Exceptions
{
    public class DuplicateAccountException : Exception
    {
        public DuplicateAccountException(string message) : base(message)
        {
        }
    }
}