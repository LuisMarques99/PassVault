﻿using System.ComponentModel.DataAnnotations;

namespace Server.Api.Dtos.Accounts
{
    public class AccountUpdateDto
    {
        [Required] public string Url { get; set; }

        [Required] public string Username { get; set; }

        [Required] public string Password { get; set; }

        public string Group { get; set; }
    }
}