using System;

namespace Server.Api.Dtos.Users
{
	public class UserReadDto
	{
		public int Id { get; set; }

		public string Username { get; set; }

		public string Email { get; set; }

		public string FirstName { get; set; }

		public string LastName { get; set; }

		public DateTime Dob { get; set; }
	}
}