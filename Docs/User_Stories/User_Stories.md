# User Stories

## Version: 1.2

## Contents

- [US_1](#us_1)
- [US_2](#us_2)
- [US_3](#us_3)

## US_1

**Story:** As a user I want to register and login into the application, logout from it and have a profile
page with the possibility to update all my personal informations.

**Acceptance criteria:** To register into the application, it is needed an email and a password. To login,
the email and the password have to match with the current ones saved in our database. To logout
and to update the personal informations it is only needed to be logged in. To update the password,
it is needed to follow a link sent to email with a 5 minute-validity.

## US_2

**Story:** As a user I want to add new passwords into my account and be capable to update and remove
them. Also I must be able to access directly into my account from the application.

**Acceptance criteria:** To add new passwords there's only needed to be logged in. To update or
remove a password, it is needed to add it first. It will be possible to log in directly into the password
account from the application but only for a certain platforms at first.

## US_3

**Story:** As a user I want to be able to create password groups, update and remove them and also able
to add passwords to it and to remove them from it.

**Acceptance criteria:** To create a password group there's only needed to be logged in. To update or
remove it, it is needed to add it first. To add passwords to the group, the password must be created
first as well as the group. To remove the passwords from the group it is needed to add them first.
