export class Password {
    id: string;
    userid: string;
    url: string;
    username: string;
    password: string;
    group: string;
}