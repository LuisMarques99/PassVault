import json
import time
from selenium import webdriver

PLATFORM_NAME = "gitlab"

url = ""
username_field = ""
password_field = ""
form_button = ""
browser_check_time = 0
username = ""
password = ""


with open("platforms.json", "r") as platforms_file:
    platforms = json.load(platforms_file)
    for platform in platforms:
        if platform["name"] == PLATFORM_NAME:
            url = platform["url"]
            username_field = platform["username_field"]
            password_field = platform["password_field"]
            form_button = platform["form_button"]
            browser_check_time = platform["browser_check_time"]
            break
platforms_file.close()

with open("accounts.json", "r") as accounts_file:
    accounts = json.load(accounts_file)
    for account in accounts:
        if account["name"] == PLATFORM_NAME:
            username = account["username"]
            password = account["password"]
            break
accounts_file.close()


driver = webdriver.Chrome()
driver.get(url)

time.sleep(browser_check_time)

username_element = driver.find_element_by_xpath(username_field)
username_element.send_keys(username)

password_element = driver.find_element_by_xpath(password_field)
password_element.send_keys(password)

buttom_element = driver.find_element_by_xpath(form_button)
buttom_element.click()
