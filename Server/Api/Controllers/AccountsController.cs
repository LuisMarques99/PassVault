﻿using System;
using System.Collections.Generic;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.JsonPatch;
using Microsoft.AspNetCore.Mvc;
using Server.Api.Data.Accounts;
using Server.Api.Dtos.Accounts;
using Server.Api.Helpers.Exceptions;
using Server.Api.Models;

namespace Server.Api.Controllers
{
    [Authorize]
    [ApiController]
    [Route("api/v1/[controller]")]
    public class AccountsController : ControllerBase
    {
        private readonly IAccountsRepository _repository;
        private readonly IMapper _mapper;

        public AccountsController(IAccountsRepository repository, IMapper mapper)
        {
            _repository = repository;
            _mapper = mapper;
        }

        // GET api/v1/accounts
        [HttpGet]
        public ActionResult<IEnumerable<AccountReadDto>> GetAllAccounts()
        {
            var accounts = _repository.GetAllAccounts();
            var accountsReadDto = _mapper.Map<IEnumerable<AccountReadDto>>(accounts);

            return Ok(accountsReadDto);
        }

        // GET api/v1/accounts/forUser/{userId}
        [HttpGet("forUser/{userId}")]
        public ActionResult<IEnumerable<AccountReadDto>> GetAllAccountsByUserid(int userId)
        {
            IEnumerable<Account> accounts;
            try
            {
                accounts = _repository.GetAllAccountsByUserId(userId);
            }
            catch (InvalidUserException e)
            {
                return BadRequest(new {message = e.Message});
            }

            var accountsReadDto = _mapper.Map<IEnumerable<AccountReadDto>>(accounts);

            return Ok(accountsReadDto);
        }

        // GET api/v1/accounts/{id}
        [HttpGet("{id}", Name = "GetAccountById")]
        public ActionResult<AccountReadDto> GetAccountById(int id)
        {
            var account = _repository.GetAccountById(id);

            if (account == null) return NotFound();

            var accountReadDto = _mapper.Map<AccountReadDto>(account);

            return Ok(accountReadDto);
        }

        // POST api/v1/accounts
        [HttpPost]
        public ActionResult<AccountReadDto> CreateAccount(AccountCreateDto accountCreateDto)
        {
            if (accountCreateDto == null) return NoContent();

            var account = _mapper.Map<Account>(accountCreateDto);

            try
            {
                _repository.CreateAccount(account);
            }
            catch (Exception e) when (e is DuplicateAccountException || e is InvalidUserException)
            {
                return BadRequest(new {message = e.Message});
            }

            _repository.SaveChanges();

            var accountReadDto = _mapper.Map<AccountReadDto>(account);

            return CreatedAtRoute(nameof(GetAccountById), new {accountReadDto.Id}, accountReadDto);
        }

        // PUT api/v1/accounts/{id}
        [HttpPut("{id}")]
        public ActionResult UpdateAccount(int id, AccountUpdateDto accountUpdateDto)
        {
            var account = _repository.GetAccountById(id);

            if (account == null) return NotFound();

            _mapper.Map(accountUpdateDto, account);

            try
            {
                _repository.UpdateAccount(account);
                _repository.SaveChanges();
            }
            catch (Exception e) when (e is DuplicateAccountException || e is InvalidUserException)
            {
                return BadRequest(new {message = e.Message});
            }

            return NoContent();
        }

        // PATCH api/v1/accounts/{id}
        [HttpPatch("{id}")]
        public ActionResult PartialUpdateAccount(int id, JsonPatchDocument<AccountUpdateDto> patchDocument)
        {
            var account = _repository.GetAccountById(id);

            if (account == null) return NotFound();

            var accountUpdateDto = _mapper.Map<AccountUpdateDto>(account);

            patchDocument.ApplyTo(accountUpdateDto, ModelState);

            if (!TryValidateModel(accountUpdateDto)) return ValidationProblem(ModelState);

            _mapper.Map(accountUpdateDto, account);

            try
            {
                _repository.UpdateAccount(account);
                _repository.SaveChanges();
            }
            catch (Exception e) when (e is DuplicateAccountException || e is InvalidUserException)
            {
                return BadRequest(new {message = e.Message});
            }

            return NoContent();
        }
        
        // DELETE api/v1/accounts/{id}
        [HttpDelete("{id}")]
        public ActionResult DeleteAccount(int id)
        {
            var account = _repository.GetAccountById(id);

            if (account == null) return NotFound();
            
            _repository.DeleteAccount(account);
            _repository.SaveChanges();

            return NoContent();
        }
    }
}