using System;
using System.ComponentModel.DataAnnotations;

namespace Server.Api.Models
{
    public class User
    {
        [Key] [Required] public int Id { get; set; }

        [Required] public string Username { get; set; }

        [Required] public string Email { get; set; }

        [Required] public string Password { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public Nullable<DateTime> Dob { get; set; }
    }
}