﻿namespace Server.Api.Dtos.Accounts
{
    public class AccountReadDto
    {
        public int Id { get; set; }

        public int UserId { get; set; }

        public string Url { get; set; }

        public string Username { get; set; }

        public string Group { get; set; }
    }
}