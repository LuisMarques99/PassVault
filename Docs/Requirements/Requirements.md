# Requirements

## Version: 1.4

## Contents

- [Functional](#functional)
- [Non Functional](#non-functional)

## Functional

### FR_1.1

**Name:** Login

**User Story:** US_1

**Category:** Application session

**Description:** Login of the user into the application

**Priority:** High

### FR_1.2

**Name:** Logout

**User Story:** US_1

**Category:** Application session

**Description:** Logout of the user from the application

**Priority:** High

### FR_1.3

**Name:** Profile page

**User Story:** US_1

**Category:** Application session

**Description:** Current user profile page

**Priority:** High

### FR_2.1

**Name:** Register new user

**User Story:** US_1

**Category:** User management

**Description:** Registation of a new user into the application

**Priority:** High

### FR_2.2

**Name:** Update user profile

**User Story:** US_1

**Category:** User management

**Description:** Update general settings of user profile

**Priority:** High

### FR_2.3

**Name:** Update user password

**User Story:** US_1

**Category:** User management

**Description:** Update user password through a link sent to email with a 5-minute validity

**Priority:** High

### FR_3.1

**Name:** Add password

**User Story:** US_2

**Category:** Password management

**Description:** A registered user adds a new password

**Priority:** High

### FR_3.2

**Name:** Update password

**User Story:** US_2

**Category:** Password management

**Description:** A registered user updates a password

**Priority:** High

### FR_3.3

**Name:** Delete password

**User Story:** US_2

**Category:** Password management

**Description:** A registered user deletes a password

**Priority:** High

### FR_3.4

**Name:** Login into password account

**User Story:** US_2

**Category:** Password management

**Description:** A registered user logs in into his account with his username/email and password

**Priority:** High

### FR_4.1

**Name:** Create new password group

**User Story:** US_3

**Category:** Password groups management

**Description:** A registered user creates a new password group

**Priority:** Medium

### FR_4.2

**Name:** Update password group

**User Story:** US_3

**Category:** Password groups management

**Description:** A registered user updates a password group

**Priority:** Medium

### FR_4.3

**Name:** Delete password group

**User Story:** US_3

**Category:** Password groups management

**Description:** A registered user deletes a password group

**Priority:** Medium

### FR_4.4

**Name:** Add password to password group

**User Story:** US_3

**Category:** Password groups management

**Description:** A registered user adds a password to a password group

**Priority:** Medium

### FR_4.5

**Name:** Remove password from password group

**User Story:** US_3

**Category:** Password groups management

**Description:** A registered user removes a password from a password group

**Priority:** Medium

## Non functional

### NFR_1.1

**Name:** Well documented REST API

**Category:** REST API

**Description:** Well documented REST API, so that it is easy to use and understand by other developers. Using Swagger for this one.

**Priority:** High

### NFR_2.1

**Name:** Web app with good usability

**Category:** Web application

**Description:** User friendly web application with great usability

**Priority:** High

### NFR_2.2

**Name:** Responsive web app

**Category:** Web application

**Description:** Web application responsive, adapted to desktop and mobile

**Priority:** High
